#include <stdlib.h>

/* Portability definitions. */

/* Misc. definitions. */

/* Use make DOUBLE=1 in large configurations with counts > 1M
 * where 24 bits of floating_t mantissa become insufficient. */
#ifdef DOUBLE
#  define floating_t double
#  define PRIfloating "%lf"
#else
#  define floating_t float
#  define PRIfloating "%f"
#endif

#define likely(x) __builtin_expect(!!(x), 1)
#define unlikely(x) __builtin_expect((x), 0)

static inline void *
malloc2(size_t size)
{
	void *p = malloc(size);
	return p;
}

static inline void *
calloc2(size_t nmemb, size_t size)
{
	void *p = calloc(nmemb, size);
	return p;
}

//#define malloc2(size)        checked_malloc((size), __FILE__, __LINE__, __func__)
//#define calloc2(nmemb, size) checked_calloc((nmemb), (size), __FILE__, __LINE__, __func__)