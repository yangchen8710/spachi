#include <stdio.h>
#include <time.h>

#include "cuct.h"
void print_board(struct board *b);
int main(){
	srand((int)time(NULL)); 
	board *b= board_init();
	board_handicap_stone(b ,3, 3,S_BLACK);
	board_handicap_stone(b ,3, 4,S_WHITE);
	board_handicap_stone(b ,4, 4,S_BLACK);
	board_handicap_stone(b ,4, 3,S_WHITE);
	//board_handicap_stone(b ,2, 4,S_BLACK);
	enum stone color = S_BLACK;

	int capture_flag =0;
	struct move m;
	bool suicide;
	do{
		m.color = color;
		m.coord = select_best_uct(b,color);
		//printf("capture_flag = %d\n",capture_flag);
		board_play(b, &m,&capture_flag);
		print_board(b);
		suicide =false;
		suicide = (m.coord != pass && !group_at(b, m.coord));
		if (suicide)
		{
		if (color==S_BLACK) capture_flag = -1;
		else if (color==S_WHITE) capture_flag = 1;
		}

		if (capture_flag==1)
		{
			printf("%s\n","black win" );
			break;
		}else if (capture_flag==-1)
		{
			printf("%s\n","white win" );
			break;
		}
		color = stone_other(color);
	}while(b->last_move.coord!=pass||b->last_move2.coord!=pass);

	if (capture_flag==0)
	{
		printf("%s\n","white win" );
	}
	if (suicide)
	{
		printf("%s\n","opponent suicided" );
	}
	

}

void print_board(struct board *b){
	for (int i = 0; i < b->size; ++i){
		for (int j = 0; j < b->size; ++j)
		{
			if (b->b[i*b->size+j]==S_NONE)
				printf("%s",". ");
			else if (b->b[i*b->size+j]==S_OFFBOARD)
				printf("%s","# " );
			else if (b->b[i*b->size+j]==S_BLACK)
				printf("%s","x " );
			else if (b->b[i*b->size+j]==S_WHITE)
				printf("%s","o " );
		}
		printf("\n");
	}
}