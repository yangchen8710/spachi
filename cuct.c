#include <stdio.h>
#include "math.h"
#include "cuct.h"

#define UCT_LOOP 100000

coord_t select_best_uct(struct board *b,enum stone color)
{
  coord_t gc = NULL;
  foreach_point(b) {
    if(board_at(b,c)==stone_other(color))
    if(b->gi[(group_at(b, c))].libs==1){
          gc = b->gi[(group_at(b, c))].lib[0];
          break;
        }
  } foreach_point_end;
  if (gc!=NULL)return gc;

  foreach_point(b) {
    if(board_at(b,c)==color)
    if(b->gi[(group_at(b, c))].libs==1){
          gc = b->gi[(group_at(b, c))].lib[0];
          break;
        }
  } foreach_point_end;
  if (gc!=NULL)return gc;


  TREE tree;
  NODE *root;
  tree.board = b;
  tree.max_loop = UCT_LOOP;
  tree.root_color = color;

  root = expand_node(b,NULL,color);
  tree.root = root;

  int i;
  for (i=0; i<UCT_LOOP; i++) {
    //printf("sim_time  = %d\n",i );
    struct board b2;
    tree.board = &b2;
    board_copy(&b2, b);
    search_uct(&tree,&b2,color,root);
    board_done_noalloc(&b2);
    color = stone_other(color);
  }


  NODE *best_n = NULL;
  int max = -999;

  for (NODE *ni = root; ni; ni = ni->sibling){
    int temp = ni->games;
    if ( temp > max ){
      best_n = ni;
      max = temp;
      //printf("%d\n",max );
      }
  }
  //printf("best_n  = %d\n", best_n->coord);

  //int ret_z = pN->child[best_i].z;
  //printf("z=%2d,rate=%.4f,games=%d,playouts=%d,nodes=%d\n",get81(ret_z),pN->child[best_i].rate,max,all_playouts,node_num);
  return best_n->coord;
}

NODE *expand_node(struct board *b,NODE *parent,enum stone color){
  coord_t pos[82];//9x9+pass
  NODE *np[82];
  int plen = 0;

  foreach_free_point(b) {
    //assert(board_at(b, c) == S_NONE);
    if (!board_is_valid_play(b, color, c))
      continue;
    pos[plen++]=c;
    
  } foreach_free_point_end;
  pos[plen++] = pass;// PASSも追加

  for (int i = 0; i < plen; ++i)
  {
    NODE *tp = alloc_node();
    setup_node(tp,parent,pos[i]);
    np[i] = tp;
  }

  for (int i = 0; i < plen-1; ++i)
  {
    np[i]->sibling = np[i+1];
  }

  if(parent!=NULL)
    parent->children = np[0];

  return np[0];
}

NODE *
alloc_node()
{
  struct tree_node *n ;
  n = calloc2(1,sizeof(*n));
  return n;
}

void
setup_node(NODE *n, NODE *parent,coord_t coord)
{
  n->coord = coord;
  n->games = 0;
  n->bwin = 0;
  n->wwin = 0;
  n->parent = parent;
  n->sibling = NULL;
  n->children = NULL;
}

int search_uct(TREE *t,struct board *b,enum stone color,NODE *n)
{
  int capture_f = 0;
//re_try:
  // UCBが一番高い手を選ぶ
  NODE *select_node = NULL;
  double max_ucb = -999;
  int i;
  
  for (NODE *ni = n; ni; ni = ni->sibling){
    double ucb = 0;
    if (ni->games == 0)
    {
      ucb = 10000 + rand()%80;
    }else{
      const double C = 0.31;
      if (ni->parent!=NULL)
      {
        if (color==S_BLACK)
        {
          ucb = 1.0*(1.0*ni->bwin-ni->wwin)/ni->games + C * sqrt( log(ni->parent->games) / ni->games );
        }else if(color==S_WHITE){
          ucb = 1.0*(1.0*ni->wwin-ni->bwin)/ni->games + C * sqrt( log(ni->parent->games) / ni->games );
        }
      }else{
        if (color==S_BLACK)
        {
          ucb = 1.0*(1.0*ni->bwin-ni->wwin)/ni->games + C * sqrt( log(t->games) / ni->games );
        }else if(color==S_WHITE){
          ucb = 1.0*(1.0*ni->wwin-ni->bwin)/ni->games + C * sqrt( log(t->games) / ni->games );
        }
        
      }
    }
    if ( ucb > max_ucb ) {
      max_ucb = ucb;
      select_node = ni;
    }
  }
  if ( select_node == NULL ) { printf("Err! select\n"); exit(0); }

  struct move m;
  m.color = color;
  m.coord = select_node->coord;
  board_play(b, &m,&capture_f);
  bool suicide = false;

  suicide = (m.coord != pass && !group_at(b, m.coord));
  if (suicide)
  {
    if (color==S_BLACK)
    {
      capture_f = -1;
    }else if (color==S_WHITE){
      capture_f = 1;
    }

  }


  if (capture_f!=0)
  {
    // 勝率を更新
    if (capture_f==1)
    {
      select_node->bwin++;
    }else if (capture_f==-1){
      select_node->wwin++;
    }

    select_node->games++;
    //n->games++;   // この手の回数を更新
    t->games++;  // 全体の回数を更新
    return capture_f;  
  }

  if (select_node->games==0)
    capture_f = cplayout(b, stone_other(color));
  else{
    if (select_node->children == NULL ) 
      expand_node(b,select_node,stone_other(color));
    capture_f = search_uct(t,b,stone_other(color),select_node->children);
  }

  // 勝率を更新
  if (capture_f==1)
  {
    select_node->bwin++;
  }else if (capture_f==-1){
    select_node->wwin++;
  }

  select_node->games++;
  //n->games++;   // この手の回数を更新
  t->games++;  // 全体の回数を更新
  return capture_f;  
}
