#include <stdio.h>
#include "playout.h"

typedef struct tree_node {
	struct tree_node *parent, *sibling, *children;

	/* coord is usually coord_t, but this is very space-sensitive. */
#define node_coord(n) ((int) (n)->coord)
	coord_t coord;
	unsigned int games;
	unsigned int bwin;
	unsigned int wwin;
}NODE;

typedef struct tree {
	struct board *board;
	struct tree_node *root;
	unsigned int games;
	enum stone root_color;
	int max_loop;
}TREE;

coord_t select_best_uct(struct board *b,enum stone color);
NODE *expand_node(struct board *b,NODE *parent,enum stone color);
NODE *alloc_node();
void setup_node(NODE *n, NODE *parent,coord_t coord);
int search_uct(TREE *t,struct board *b,enum stone color,NODE *n);
