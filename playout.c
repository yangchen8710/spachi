#include <stdio.h>

#include "playout.h"

void print_board(struct board *b);
int cplayout(struct board *b,enum stone color)
{
	struct board b2;
	board_copy(&b2, b);
	int capture_flag = 0;

	struct move m;
	do{
		m.color = color;
		m.coord = genmove_rand(&b2,color,&capture_flag);
		//printf("capture_flag = %d\n",capture_flag);
		//print_board(&b2);
		if (capture_flag!=0)
		{
			break;
		}
		color = stone_other(color);
	}while(b2.last_move.coord!=pass||b2.last_move2.coord!=pass);
	board_done_noalloc(&b2);

	return capture_flag;
}

coord_t *
genmove_rand(struct board *b,enum stone color,int *capture_flag){
	coord_t coord;
	int i = 0; bool suicide = false;

	do {
	board_play_random(b, color, &coord,capture_flag);

	suicide = (coord != pass && !group_at(b, coord));
	} while (suicide && i++ < 100);
	if (suicide)
	{
		if (color==S_BLACK)
		{
			*capture_flag = -1;
		}else if (color==S_WHITE){
			*capture_flag = 1;
		}

	}

	return (suicide ? pass : coord);
}

