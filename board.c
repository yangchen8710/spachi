#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#define DEBUG
#include "board.h"

static void board_trait_recompute(struct board *board, coord_t coord);

#define profiling_noinline
#define BOARD_MAX_SIZE 19
#define gi_granularity 4
#define gi_allocsize(gids) ((1 << gi_granularity) + ((gids) >> gi_granularity) * (1 << gi_granularity))


static void
board_setup(struct board *b)
{
	memset(b, 0, sizeof(*b));

	struct move m = { pass, S_NONE };
	b->last_move = b->last_move2 = b->last_move3 = b->last_move4 = b->last_ko = b->ko = m;
}

struct board *
board_init()
{
	struct board *b = malloc2(sizeof(struct board));
	board_setup(b);
	// Default setup
	b->size = 6 + 2;
	board_clear(b);
	return b;
}

static size_t
board_alloc(struct board *board)
{
	/* We do not allocate the board structure itself but we allocate
	 * all the arrays with board contents. */

	int bsize = board_size2(board) * sizeof(*board->b);
	int gsize = board_size2(board) * sizeof(*board->g);
	int fsize = board_size2(board) * sizeof(*board->f);
	int nsize = board_size2(board) * sizeof(*board->n);
	int psize = board_size2(board) * sizeof(*board->p);
	int gisize = board_size2(board) * sizeof(*board->gi);
//WANT_BOARD_C
	int csize = board_size2(board) * sizeof(*board->c);

//BOARD_SPATHASH
	int ssize = 0;
//BOARD_PAT3
	int p3size = 0;

//BOARD_TRAITS
	int tsize = board_size2(board) * sizeof(*board->t);
	int tqsize = board_size2(board) * sizeof(*board->t);

	int cdsize = board_size2(board) * sizeof(*board->coord);

	size_t size = bsize + gsize + fsize + psize + nsize + gisize + csize + ssize + p3size + tsize + tqsize + cdsize;
	void *x = malloc2(size);

	/* board->b must come first */
	board->b = x; x += bsize;
	board->g = x; x += gsize;
	board->f = x; x += fsize;
	board->p = x; x += psize;
	board->n = x; x += nsize;
	board->gi = x; x += gisize;

	board->c = x; x += csize;
//BOARD_TRAITS
	board->t = x; x += tsize;
	board->tq = x; x += tqsize;

	board->coord = x; x += cdsize;

	return size;
}

struct board *
board_copy(struct board *b2, struct board *b1)
{
	memcpy(b2, b1, sizeof(struct board));

	size_t size = board_alloc(b2);
	memcpy(b2->b, b1->b, size);

	return b2;
}

void
board_done_noalloc(struct board *board)
{
	if (board->b) free(board->b);
}

void
board_done(struct board *board)
{
	board_done_noalloc(board);
	free(board);
}

void
board_resize(struct board *board, int size)
{
#ifdef BOARD_SIZE
	assert(board_size(board) == size + 2);
#endif
	board->size = size + 2 /* S_OFFBOARD margin */;
	board->size2 = board_size(board) * board_size(board);

	board->bits2 = 1;
	while ((1 << board->bits2) < board->size2) board->bits2++;

	if (board->b)
		free(board->b);

	size_t asize = board_alloc(board);
	memset(board->b, 0, asize);
}


static void
board_init_data(struct board *board)
{
	int size = board_size(board);

	board_setup(board);
	board_resize(board, size - 2 /* S_OFFBOARD margin */);

	/* Setup neighborhood iterators */
	board->nei8[0] = -size - 1; // (-1,-1)
	board->nei8[1] = 1;
	board->nei8[2] = 1;
	board->nei8[3] = size - 2; // (-1,0)
	board->nei8[4] = 2;
	board->nei8[5] = size - 2; // (-1,1)
	board->nei8[6] = 1;
	board->nei8[7] = 1;
	board->dnei[0] = -size - 1;
	board->dnei[1] = 2;
	board->dnei[2] = size*2 - 2;
	board->dnei[3] = 2;

	/* Set up coordinate cache */
	foreach_point(board) {
		board->coord[c][0] = c % board_size(board);
		board->coord[c][1] = c / board_size(board);
	} foreach_point_end;

	/* Draw the offboard margin */
	int top_row = board_size2(board) - board_size(board);
	int i;
	for (i = 0; i < board_size(board); i++)
		board->b[i] = board->b[top_row + i] = S_OFFBOARD;
	for (i = 0; i <= top_row; i += board_size(board))
		board->b[i] = board->b[board_size(board) - 1 + i] = S_OFFBOARD;

	foreach_point(board) {
		coord_t coord = c;
		if (board_at(board, coord) == S_OFFBOARD)
			continue;
		foreach_neighbor(board, c, {
			inc_neighbor_count_at(board, coord, board_at(board, c));
		} );
	} foreach_point_end;

	/* All positions are free! Except the margin. */
	for (i = board_size(board); i < (board_size(board) - 1) * board_size(board); i++)
		if (i % board_size(board) != 0 && i % board_size(board) != board_size(board) - 1)
			board->f[board->flen++] = i;

	/* Initialize traits. */
	foreach_point(board) {
		trait_at(board, c, S_BLACK).cap = 0;
		trait_at(board, c, S_WHITE).cap = 0;
		trait_at(board, c, S_BLACK).cap1 = 0;
		trait_at(board, c, S_WHITE).cap1 = 0;
	} foreach_point_end;

}

void
board_clear(struct board *board)
{
	int size = board_size(board);
	floating_t komi = board->komi;

	enum go_ruleset rules = board->rules;

	board_done_noalloc(board);

	static struct board bcache[BOARD_MAX_SIZE + 2];
	assert(size > 0 && size <= BOARD_MAX_SIZE + 2);
	if (bcache[size - 1].size == size) {
		board_copy(board, &bcache[size - 1]);
	} else {
		board_init_data(board);
		board_copy(&bcache[size - 1], board);
	}

	board->komi = komi;

	board->rules = rules;

}


static void
board_trait_recompute(struct board *board, coord_t coord)
{
	int sfb = -1, sfw = -1;
#ifdef BOARD_TRAIT_SAFE
	sfb = trait_at(board, coord, S_BLACK).safe = board_trait_safe(board, coord, S_BLACK);
	sfw = trait_at(board, coord, S_WHITE).safe = board_trait_safe(board, coord, S_WHITE);
#endif
}

/* Recompute traits for dirty points that we have previously touched
 * somehow (libs of their neighbors changed or so). */
static void
board_traits_recompute(struct board *board)
{
	for (int i = 0; i < board->tqlen; i++) {
		coord_t coord = board->tq[i];
		trait_at(board, coord, S_BLACK).dirty = false;
		if (board_at(board, coord) != S_NONE)
			continue;
		board_trait_recompute(board, coord);
	}
	board->tqlen = 0;
}

/* Queue traits of given point for recomputing. */
static void
board_trait_queue(struct board *board, coord_t coord)
{
	if (trait_at(board, coord, S_BLACK).dirty)
		return;
	board->tq[board->tqlen++] = coord;
	trait_at(board, coord, S_BLACK).dirty = true;
}


void
board_handicap_stone(struct board *board, int x, int y,enum stone color)
{
	struct move m;
	m.color = color; 
	m.coord = coord_xy(board, x, y);

	board_play(board, &m ,NULL);
	/* Simulate white passing; otherwise, UCT search can get confused since
	 * tree depth parity won't match the color to move. */
	board->moves++;

	char *str = coord2str(m.coord, board);

	free(str);
}

void
board_handicap(struct board *board, int stones)
{
	int margin = 3 + (board_size(board) >= 13);
	int min = margin;
	int mid = board_size(board) / 2;
	int max = board_size(board) - 1 - margin;
	const int places[][2] = {
		{ min, min }, { max, max }, { min, max }, { max, min },
		{ min, mid }, { max, mid },
		{ mid, min }, { mid, max },
		{ mid, mid },
	};

	board->handicap = stones;

	if (stones == 5 || stones == 7) {
		board_handicap_stone(board, mid, mid,S_BLACK);
		stones--;
	}

	int i;
	for (i = 0; i < stones; i++)
		board_handicap_stone(board, places[i][0], places[i][1],S_BLACK);
}
//coord_t coord2 = c;

static void
board_capturable_add(struct board *board, group_t group, coord_t lib, bool onestone)
{

	/* Increase capturable count trait of my last lib. */
	enum stone capturing_color = stone_other(board_at(board, group));
	assert(capturing_color == S_BLACK || capturing_color == S_WHITE);
	foreach_neighbor(board, lib, {
		trait_at(board, lib, capturing_color).cap += (group_at(board, c) == group);
		trait_at(board, lib, capturing_color).cap1 += (group_at(board, c) == group && onestone);
	});
	board_trait_queue(board, lib);


	/* Update the list of capturable groups. */
	assert(group);
	assert(board->clen < board_size2(board));
	board->c[board->clen++] = group;
}

static void
board_capturable_rm(struct board *board, group_t group, coord_t lib, bool onestone)
{
	/* Decrease capturable count trait of my previously-last lib. */
	enum stone capturing_color = stone_other(board_at(board, group));
	assert(capturing_color == S_BLACK || capturing_color == S_WHITE);
	foreach_neighbor(board, lib, {
		trait_at(board, lib, capturing_color).cap -= (group_at(board, c) == group);
		trait_at(board, lib, capturing_color).cap1 -= (group_at(board, c) == group && onestone);
	});
	board_trait_queue(board, lib);
	/* Update the list of capturable groups. */
	for (int i = 0; i < board->clen; i++) {
		if (unlikely(board->c[i] == group)) {
			board->c[i] = board->c[--board->clen];
			return;
		}
	}
	fprintf(stderr, "rm of bad group %d\n", group_base(group));
	assert(0);
}

static void
board_atariable_add(struct board *board, group_t group, coord_t lib1, coord_t lib2)
{
	board_trait_queue(board, lib1);
	board_trait_queue(board, lib2);
}
static void
board_atariable_rm(struct board *board, group_t group, coord_t lib1, coord_t lib2)
{
	board_trait_queue(board, lib1);
	board_trait_queue(board, lib2);
}

static void
board_group_addlib(struct board *board, group_t group, coord_t coord)
{
	struct group *gi = &board_group_info(board, group);
	bool onestone = group_is_onestone(board, group);
	if (gi->libs < GROUP_KEEP_LIBS) {
		for (int i = 0; i < GROUP_KEEP_LIBS; i++) {
			if (gi->lib[i] == coord)
				return;
		}
		if (gi->libs == 0) {
			board_capturable_add(board, group, coord, onestone);
		} else if (gi->libs == 1) {
			board_capturable_rm(board, group, gi->lib[0], onestone);
			board_atariable_add(board, group, gi->lib[0], coord);
		} else if (gi->libs == 2) {
			board_atariable_rm(board, group, gi->lib[0], gi->lib[1]);
		}
		gi->lib[gi->libs++] = coord;
	}
}

static void
board_group_find_extra_libs(struct board *board, group_t group, struct group *gi, coord_t avoid)
{
	/* Add extra liberty from the board to our liberty list. */
	unsigned char watermark[board_size2(board) / 8];
	memset(watermark, 0, sizeof(watermark));
#define watermark_get(c)	(watermark[c >> 3] & (1 << (c & 7)))
#define watermark_set(c)	watermark[c >> 3] |= (1 << (c & 7))

	for (int i = 0; i < GROUP_KEEP_LIBS - 1; i++)
		watermark_set(gi->lib[i]);
	watermark_set(avoid);

	foreach_in_group(board, group) {
		coord_t coord2 = c;
		foreach_neighbor(board, coord2, {
			if (board_at(board, c) + watermark_get(c) != S_NONE)
				continue;
			watermark_set(c);
			gi->lib[gi->libs++] = c;
			if (unlikely(gi->libs >= GROUP_KEEP_LIBS))
				return;
		} );
	} foreach_in_group_end;
#undef watermark_get
#undef watermark_set
}

static void
board_group_rmlib(struct board *board, group_t group, coord_t coord)
{

	struct group *gi = &board_group_info(board, group);
	bool onestone = group_is_onestone(board, group);
	for (int i = 0; i < GROUP_KEEP_LIBS; i++) {
		if (gi->lib[i] != coord)
			continue;

		coord_t lib = gi->lib[i] = gi->lib[--gi->libs];
		gi->lib[gi->libs] = 0;


		/* Postpone refilling lib[] until we need to. */
		assert(GROUP_REFILL_LIBS > 1);
		if (gi->libs > GROUP_REFILL_LIBS)
			return;
		if (gi->libs == GROUP_REFILL_LIBS)
			board_group_find_extra_libs(board, group, gi, coord);

		if (gi->libs == 2) {
			board_atariable_add(board, group, gi->lib[0], gi->lib[1]);
		} else if (gi->libs == 1) {
			board_capturable_add(board, group, gi->lib[0], onestone);
			board_atariable_rm(board, group, gi->lib[0], lib);
		} else if (gi->libs == 0)
			board_capturable_rm(board, group, lib, onestone);
		return;
	}

	/* This is ok even if gi->libs < GROUP_KEEP_LIBS since we
	 * can call this multiple times per coord. */
	return;
}


/* This is a low-level routine that doesn't maintain consistency
 * of all the board data structures. */
static void
board_remove_stone(struct board *board, group_t group, coord_t c)
{
	enum stone color = board_at(board, c);
	board_at(board, c) = S_NONE;
	group_at(board, c) = 0;
	/* We mark as cannot-capture now. If this is a ko/snapback,
	 * we will get incremented later in board_group_addlib(). */
	trait_at(board, c, S_BLACK).cap = trait_at(board, c, S_BLACK).cap1 = 0;
	trait_at(board, c, S_WHITE).cap = trait_at(board, c, S_WHITE).cap1 = 0;
	board_trait_queue(board, c);

	/* Increase liberties of surrounding groups */
	coord_t coord = c;
	foreach_neighbor(board, coord, {
		dec_neighbor_count_at(board, c, color);
		board_trait_queue(board, c);
		group_t g = group_at(board, c);
		if (g && g != group)
			board_group_addlib(board, g, coord);
	});

	board->f[board->flen++] = c;
}

static int profiling_noinline
board_group_capture(struct board *board, group_t group)
{
	int stones = 0;

	foreach_in_group(board, group) {
		board->captures[stone_other(board_at(board, c))]++;
		board_remove_stone(board, group, c);
		stones++;
	} foreach_in_group_end;

	struct group *gi = &board_group_info(board, group);
	assert(gi->libs == 0);
	memset(gi, 0, sizeof(*gi));

	return stones;
}


static void profiling_noinline
add_to_group(struct board *board, group_t group, coord_t prevstone, coord_t coord)
{
	struct group *gi = &board_group_info(board, group);
	bool onestone = group_is_onestone(board, group);

	if (gi->libs == 1) {
		/* Our group is temporarily in atari; make sure the capturable
		 * counts also correspond to the newly added stone before we
		 * start adding liberties again so bump-dump ops match. */
		enum stone capturing_color = stone_other(board_at(board, group));
		assert(capturing_color == S_BLACK || capturing_color == S_WHITE);

		coord_t lib = board_group_info(board, group).lib[0];
		if (coord_is_adjecent(lib, coord, board)) {
			trait_at(board, lib, capturing_color).cap++;
			/* This is never a 1-stone group, obviously. */
			board_trait_queue(board, lib);
		}

		if (onestone) {
			/* We are not 1-stone group anymore, update the cap1
			 * counter specifically. */
			foreach_neighbor(board, group, {
				if (board_at(board, c) != S_NONE) continue;
				trait_at(board, c, capturing_color).cap1--;
				board_trait_queue(board, c);
			});
		}
	}

	group_at(board, coord) = group;
	groupnext_at(board, coord) = groupnext_at(board, prevstone);
	groupnext_at(board, prevstone) = coord;

	foreach_neighbor(board, coord, {
		if (board_at(board, c) == S_NONE)
			board_group_addlib(board, group, c);
	});
}

static void profiling_noinline
merge_groups(struct board *board, group_t group_to, group_t group_from)
{
	struct group *gi_from = &board_group_info(board, group_from);
	struct group *gi_to = &board_group_info(board, group_to);
	bool onestone_from = group_is_onestone(board, group_from);
	bool onestone_to = group_is_onestone(board, group_to);

	/* We do this early before the group info is rewritten. */
	if (gi_from->libs == 2)
		board_atariable_rm(board, group_from, gi_from->lib[0], gi_from->lib[1]);
	else if (gi_from->libs == 1)
		board_capturable_rm(board, group_from, gi_from->lib[0], onestone_from);

	if (gi_to->libs < GROUP_KEEP_LIBS) {
		for (int i = 0; i < gi_from->libs; i++) {
			for (int j = 0; j < gi_to->libs; j++)
				if (gi_to->lib[j] == gi_from->lib[i])
					goto next_from_lib;
			if (gi_to->libs == 0) {
				board_capturable_add(board, group_to, gi_from->lib[i], onestone_to);
			} else if (gi_to->libs == 1) {
				board_capturable_rm(board, group_to, gi_to->lib[0], onestone_to);
				board_atariable_add(board, group_to, gi_to->lib[0], gi_from->lib[i]);
			} else if (gi_to->libs == 2) {
				board_atariable_rm(board, group_to, gi_to->lib[0], gi_to->lib[1]);
			}
			gi_to->lib[gi_to->libs++] = gi_from->lib[i];
			if (gi_to->libs >= GROUP_KEEP_LIBS)
				break;
next_from_lib:;
		}
	}

	if (gi_to->libs == 1) {
		coord_t lib = board_group_info(board, group_to).lib[0];

		enum stone capturing_color = stone_other(board_at(board, group_to));
		assert(capturing_color == S_BLACK || capturing_color == S_WHITE);

		/* Our group is currently in atari; make sure we properly
		 * count in even the neighbors from the other group in the
		 * capturable counter. */
		foreach_neighbor(board, lib, {
			trait_at(board, lib, capturing_color).cap += (group_at(board, c) == group_from);
			/* This is never a 1-stone group, obviously. */
		});
		board_trait_queue(board, lib);

		if (onestone_to) {
			/* We are not 1-stone group anymore, update the cap1
			 * counter specifically. */
			foreach_neighbor(board, group_to, {
				if (board_at(board, c) != S_NONE) continue;
				trait_at(board, c, capturing_color).cap1--;
				board_trait_queue(board, c);
			});
		}

	}

	coord_t last_in_group;
	foreach_in_group(board, group_from) {
		last_in_group = c;
		group_at(board, c) = group_to;
	} foreach_in_group_end;
	groupnext_at(board, last_in_group) = groupnext_at(board, group_base(group_to));
	groupnext_at(board, group_base(group_to)) = group_base(group_from);
	memset(gi_from, 0, sizeof(struct group));
}

static group_t profiling_noinline
new_group(struct board *board, coord_t coord)
{
	group_t group = coord;
	struct group *gi = &board_group_info(board, group);
	foreach_neighbor(board, coord, {
		if (board_at(board, c) == S_NONE)
			/* board_group_addlib is ridiculously expensive for us */
#if GROUP_KEEP_LIBS < 4
			if (gi->libs < GROUP_KEEP_LIBS)
#endif
			gi->lib[gi->libs++] = c;
	});

	group_at(board, coord) = group;
	groupnext_at(board, coord) = 0;

	if (gi->libs == 2)
		board_atariable_add(board, group, gi->lib[0], gi->lib[1]);
	else if (gi->libs == 1)
		board_capturable_add(board, group, gi->lib[0], true);

	return group;
}

static inline group_t
play_one_neighbor(struct board *board,
		coord_t coord, enum stone color, enum stone other_color,
		coord_t c, group_t group,int *capture_flag)
{
	enum stone ncolor = board_at(board, c);
	group_t ngroup = group_at(board, c);

	inc_neighbor_count_at(board, c, color);
	/* We can be S_NONE, in that case we need to update the safety
	 * trait since we might be left with only one liberty. */
	board_trait_queue(board, c);

	if (!ngroup)
		return group;

	board_group_rmlib(board, ngroup, coord);

	if (ncolor == color && ngroup != group) {
		if (!group) {
			group = ngroup;
			add_to_group(board, group, c, coord);
		} else {
			merge_groups(board, group, ngroup);
		}
	} else if (ncolor == other_color) {

		if (board_group_captured(board, ngroup)){
			if(color == S_BLACK)
				*capture_flag = 1;
			else *capture_flag = -1;
			board_group_capture(board, ngroup);}
	}
	return group;
}

/* We played on a place with at least one liberty. We will become a member of
 * some group for sure. */
static group_t profiling_noinline
board_play_outside(struct board *board, struct move *m, int f,int *capture_flag)
{
	coord_t coord = m->coord;
	enum stone color = m->color;
	enum stone other_color = stone_other(color);
	group_t group = 0;

	board->f[f] = board->f[--board->flen];

	foreach_neighbor(board, coord, {
		group = play_one_neighbor(board, coord, color, other_color, c, group,capture_flag);
	});

	board_at(board, coord) = color;
	if (!group)
		group = new_group(board, coord);

	board->last_move2 = board->last_move;
	board->last_move = *m;
	board->moves++;
	struct move ko = { pass, S_NONE };
	board->ko = ko;
	return group;
}

/* We played in an eye-like shape. Either we capture at least one of the eye
 * sides in the process of playing, or return -1. */
static int profiling_noinline
board_play_in_eye(struct board *board, struct move *m, int f,int *capture_flag)
{
	coord_t coord = m->coord;
	enum stone color = m->color;
	/* Check ko: Capture at a position of ko capture one move ago */
	if (color == board->ko.color && coord == board->ko.coord) {
		return -1;
	}

	struct move ko = { pass, S_NONE };

	int captured_groups = 0;

	foreach_neighbor(board, coord, {
		group_t g = group_at(board, c);
		captured_groups += (board_group_info(board, g).libs == 1);
	});

	if (captured_groups == 0) {
		return -1;
	}
	if(color == S_BLACK)
		*capture_flag = 1;
	else *capture_flag = -1;

	/* We _will_ for sure capture something. */
	assert(trait_at(board, coord, color).cap > 0);
#ifdef BOARD_TRAIT_SAFE
	assert(trait_at(board, coord, color).safe == board_trait_safe(board, coord, color));
#endif

	board->f[f] = board->f[--board->flen];
	int ko_caps = 0;
	coord_t cap_at = pass;
	foreach_neighbor(board, coord, {
		inc_neighbor_count_at(board, c, color);
		/* Originally, this could not have changed any trait
		 * since no neighbors were S_NONE, however by now some
		 * of them might be removed from the board. */
		board_trait_queue(board, c);

		group_t group = group_at(board, c);
		if (!group)
			continue;

		board_group_rmlib(board, group, coord);

		if (board_group_captured(board, group)) {
			ko_caps += board_group_capture(board, group);
			cap_at = c;
		}
	});
	if (ko_caps == 1) {
		ko.color = stone_other(color);
		ko.coord = cap_at; // unique
		board->last_ko = ko;
		board->last_ko_age = board->moves;
	}

	board_at(board, coord) = color;
	group_t group = new_group(board, coord);

	board->last_move2 = board->last_move;
	board->last_move = *m;
	board->moves++;
	board_traits_recompute(board);
	board->ko = ko;


	return !!group;
}

static int __attribute__((flatten))
board_play_f(struct board *board, struct move *m, int f,int *capture_flag)
{
	if (!board_is_eyelike(board, m->coord, stone_other(m->color))){
		/* NOT playing in an eye. Thus this move has to succeed. (This
		 * is thanks to New Zealand rules. Otherwise, multi-stone
		 * suicide might fail.) */
		group_t group = board_play_outside(board, m, f ,capture_flag);
		if (board_group_captured(board, group)) {
			board_group_capture(board, group);
		}
		board_traits_recompute(board);
		return 0;
	} else {
		return board_play_in_eye(board, m, f,capture_flag);
	}
}

int
board_play(struct board *board, struct move *m,int *capture_flag)
{
	if (is_pass(m->coord) || is_resign(m->coord)) {

		if (is_pass(m->coord) && board->rules == RULES_SIMING) {
			/* On pass, the player gives a pass stone
			 * to the opponent. */
			board->captures[stone_other(m->color)]++;
		}
		struct move nomove = { pass, S_NONE };
		board->ko = nomove;
		board->last_move4 = board->last_move3;
		board->last_move3 = board->last_move2;
		board->last_move2 = board->last_move;
		board->last_move = *m;
		return 0;
	}
	int f;
	for (f = 0; f < board->flen; f++)
		if (board->f[f] == m->coord)
			return board_play_f(board, m, f,capture_flag);

	return -1;
}

/* Undo, supported only for pass moves. This form of undo is required by KGS
 * to settle disputes on dead groups. (Undo of real moves would be more complex
 * particularly for capturing moves.) */
int board_undo(struct board *board)
{
	if (!is_pass(board->last_move.coord))
		return -1;
	if (board->rules == RULES_SIMING) {
		/* Return pass stone to the passing player. */
		board->captures[stone_other(board->last_move.color)]--;
	}
	board->last_move = board->last_move2;
	board->last_move2 = board->last_move3;
	board->last_move3 = board->last_move4;
	if (board->last_ko_age == board->moves)
		board->ko = board->last_ko;
	return 0;
}

static inline bool
board_try_random_move(struct board *b, enum stone color, coord_t *coord, int f,int *capture_flag)
{
	*coord = b->f[f];
	struct move m = { *coord, color };
	if (board_is_one_point_eye(b, *coord, color) /* bad idea to play into one, usually */
		|| !board_is_valid_move(b, &m))
		return false;
	if (m.coord == *coord) {
		return (board_play_f(b, &m, f,capture_flag) >= 0);
	} else {
		*coord = m.coord; // permit modified the coordinate
		return (board_play(b, &m,capture_flag) >= 0);
	}
}

void
board_play_random(struct board *b, enum stone color, coord_t *coord,int *capture_flag)
{
	if (unlikely(b->flen == 0))
		goto pass;

	int base;
	base  = rand()%(b->flen);
	int f;
	for (f = base; f < b->flen; f++)
		if (board_try_random_move(b, color, coord, f,capture_flag))
			return;
	for (f = 0; f < base; f++)
		if (board_try_random_move(b, color, coord, f,capture_flag))
			return;

pass:
	*coord = pass;
	struct move m = { pass, color };
	board_play(b, &m,capture_flag);
}

bool
board_is_false_eyelike(struct board *board, coord_t coord, enum stone eye_color)
{
	int color_diag_libs[S_MAX] = {0, 0, 0, 0};

	/* XXX: We attempt false eye detection but we will yield false
	 * positives in case of http://senseis.xmp.net/?TwoHeadedDragon :-( */

	foreach_diag_neighbor(board, coord) {
		color_diag_libs[(enum stone) board_at(board, c)]++;
	} foreach_diag_neighbor_end;
	/* For false eye, we need two enemy stones diagonally in the
	 * middle of the board, or just one enemy stone at the edge
	 * or in the corner. */
	color_diag_libs[stone_other(eye_color)] += !!color_diag_libs[S_OFFBOARD];
	return color_diag_libs[stone_other(eye_color)] >= 2;
}

bool
board_is_one_point_eye(struct board *board, coord_t coord, enum stone eye_color)
{
	return board_is_eyelike(board, coord, eye_color)
		&& !board_is_false_eyelike(board, coord, eye_color);
}

enum stone
board_get_one_point_eye(struct board *board, coord_t coord)
{
	if (board_is_one_point_eye(board, coord, S_WHITE))
		return S_WHITE;
	else if (board_is_one_point_eye(board, coord, S_BLACK))
		return S_BLACK;
	else
		return S_NONE;
}


floating_t
board_fast_score(struct board *board)
{
	int scores[S_MAX];
	memset(scores, 0, sizeof(scores));

	foreach_point(board) {
		enum stone color = board_at(board, c);
		if (color == S_NONE && board->rules != RULES_STONES_ONLY)
			color = board_get_one_point_eye(board, c);
		scores[color]++;
		// fprintf(stderr, "%d, %d ++%d = %d\n", coord_x(c, board), coord_y(c, board), color, scores[color]);
	} foreach_point_end;

	return board->komi + (board->rules != RULES_SIMING ? board->handicap : 0) + scores[S_WHITE] - scores[S_BLACK];
}


bool
board_set_rules(struct board *board, char *name)
{
	if (!strcasecmp(name, "japanese")) {
		board->rules = RULES_JAPANESE;
	} else if (!strcasecmp(name, "chinese")) {
		board->rules = RULES_CHINESE;
	} else if (!strcasecmp(name, "aga")) {
		board->rules = RULES_AGA;
	} else if (!strcasecmp(name, "new_zealand")) {
		board->rules = RULES_NEW_ZEALAND;
	} else if (!strcasecmp(name, "siming") || !strcasecmp(name, "simplified_ing")) {
		board->rules = RULES_SIMING;
	} else {
		return false;
	}
	return true;
}
