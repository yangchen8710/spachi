random_test:random_test.o cuct.o playout.o board.o move.o stone.o 
	g++ -g -Wall -o $@ $^ -fpermissive -w
random_test.o: cuct.h playout.h board.h move.h stone.h util.h
.c.o:
	g++ -g -Wall -O3 -march=native -c $< -fpermissive -w

clean: 
	rm -f random_test *.o
